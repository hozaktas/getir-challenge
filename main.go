package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"getir-challenge/api"
	"getir-challenge/api/validator"
	"getir-challenge/storage"

	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file", err)
	}

	// Init Storages
	inMemoryDb := storage.NewInMemoryStorage()

	timeOut, err := strconv.Atoi(os.Getenv("MONGODB_TIMEOUT"))
	if err != nil {
		log.Fatal("MONGODB_TIMEOUT invalid", err)
	}
	timeDuration := time.Duration(timeOut) * time.Second

	mongoDbClient, err := storage.NewMongoDbStorage(os.Getenv("MONGODB_URI"), timeDuration)
	if err != nil {
		log.Fatal("Could not mongoDb storage", err)
	}
	defer mongoDbClient.Disconnect(context.Background())

	// Routes
	http.HandleFunc("/mongo/records", validator.ValidateHTTPMethod(http.MethodPost, api.GetRecordsHandler(mongoDbClient, os.Getenv("MONGODB_DBNAME"))))
	http.HandleFunc("/memory/create", validator.ValidateHTTPMethod(http.MethodPost, api.SetInMemoryDbHandler(inMemoryDb)))
	http.HandleFunc("/memory/fetchdata", validator.ValidateHTTPMethod(http.MethodGet, api.GetInMemoryDbHandler(inMemoryDb)))

	// Run Server
	fmt.Println("HttpServer is running on :" + os.Getenv("PORT"))
	http.ListenAndServe("0.0.0.0:"+os.Getenv("PORT"), nil)
}
