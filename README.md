# Getir Challenge

![getir](https://avatars.githubusercontent.com/u/16684043?s=200&v=4 "Getir Bi Mutluluk")

**Demo Project:** 
[https://getir-challenge-2021.herokuapp.com/](https://getir-challenge-2021.herokuapp.com/)


To run our project, we first create and edit the .env file for ourselves.

```
cp .env_example .env
```

After filling our own information as below, our project is ready to work.

```
PORT // Determines which port the http server will run on
MONGODB_URI // Mongodb connection URI
MONGODB_DBNAME // Database name to access
MONGODB_TIMEOUT // database timeout limit
```

#### Example .env
```
PORT=8080
MONGODB_URI=mongodb+srv://example.net/db
MONGODB_DBNAME=example
MONGODB_TIMEOUT=5
```


### Api Routes

#### Get Records [POST]
> /mongo/records

It filters the `createdAt` date in the database within the date ranges specified via Mongodb, and after sum the data in the `counts` field, filters it as min-max and returns the results.

Example Request
```json
{
    "startDate": "2016-01-26", 
    "endDate": "2016-02-02", 
    "minCount": 2800, 
    "maxCount": 3000
}
```

Example Success Response
```json
{
    "code":0,
    "msg":"Success",
    "records":[
        {
            "createdAt":"2016-01-28T07:10:33.558Z",
            "key":"NOdGNUDn",
            "totalCount":2813
        },
        {
            "createdAt":"2016-01-29T01:59:53.494Z",
            "key":"bxoQiSKL", 
            "totalCount":2991
        }
    ]
}
```

Example Fail Response
```json
{
    "code": 100,
    "msg": "Invalid request method.",
}
```

___


#### SET Key - Value Data [POST]
> /memory/create

Records incoming requests in key value format to memory

Example Request
```json
{
    "key":"active-tabs",
    "value":"getir"
}
```

Example Success Response
```json
{
    "code":0,
    "msg":"Success", 
    "key":"active-tabs",
    "value":"getir"
}
```

Example Fail Response
```json
{
    "code": 101,
    "msg": "Invalid request param(s). Key or Value length zero.",
}
```

___


#### Get Key - Value Data [GET]
> /memory/fetchdata

Example Request
```
GET http://127.0.0.1:8080/memory/fetchdata?key=active-tabs
```

Example Success Response
```json
{
    "code":0,
    "msg":"Success", 
    "key":"active-tabs",
    "value":"getir"
}
```

Example Fail Response
```json
{
    "code": 102,
    "msg": "Data not found.",
}
```

### Custom Error Codes

Code | Description
--- | --- 
**0** | All transactions done successfully 
**101** | Request was made with a method that is not allowed on the api end
**102** | A request was made to the API end with a missing or wrong parameter
**103** | Data not found in *InMemoryDb*
**104** | The request was made in an invalid date format for the `startDate` parameter
**105** | The request was made in an invalid date format for the `endDate` parameter
**106** | startDate - endDate date range is invalid
**107** | minCount - maxCount range is invalid
**108** | Failed to get data over mongo db

___

### Testing
```
ok  	getir-challenge/api	2.219s	coverage: 95.3% of statements
ok  	getir-challenge/api/helper	0.131s	coverage: 100.0% of statements
ok  	getir-challenge/api/validator	0.197s	coverage: 100.0% of statements
ok  	getir-challenge/storage	0.190s	coverage: 95.0% of statements
```

### Benchmark
```
goos: darwin
goarch: amd64
pkg: getir-challenge/storage
BenchmarkInMemoryDbGet-12    	64352689	        19.1 ns/op	       0 B/op	       0 allocs/op
BenchmarkInMemoryDbSet-12    	50346258	        23.3 ns/op	       0 B/op	       0 allocs/op
```

### Log & Monitor
```
HttpServer is running on :8080
2021/06/06 17:06:14 GetRecords: Invalid request param(s). {  0 0}
2021/06/06 17:08:32 SetInMemoryDb: Invalid request param(s). Key or Value length zero. {xyz }
```