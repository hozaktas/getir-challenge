package helper

import (
	"encoding/json"
	"log"
	"net/http"

	"getir-challenge/api/types"
)

const (
	ResponseCodeSuccess = 0

	ResponseCodeErrorInvalidMethod = iota + 100
	ResponseCodeErrorInvalidRequest
	ResponseCodeErrorInMemoryDbKeyNotFound
	ResponseCodeErrorRecordsStartdateFormat
	ResponseCodeErrorRecordsEnddateFormat
	ResponseCodeErrorRecordsStartEndDiff
	ResponseCodeErrorRecordsMinMaxDiff
	ResponseCodeErrorRecordsDbErr
)

// FailedResponseHandler all rejected api requests are response here
func FailedResponseHandler(res http.ResponseWriter, statusCode int, apiErrCode int, apiErrMsg string) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(statusCode)
	data := types.GeneralResponseType{
		Code: apiErrCode,
		Msg:  apiErrMsg,
	}
	err := json.NewEncoder(res).Encode(data)
	if err != nil {
		log.Println("FailedResponseHandler: json encode", err)
	}
}

// SuccessResponseHandler all allow api requests are response here
func SuccessResponseHandler(res http.ResponseWriter, data interface{}) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	err := json.NewEncoder(res).Encode(data)
	if err != nil {
		log.Println("SuccessResponseHandler: json encode", err)
	}
}
