package helper_test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"getir-challenge/api/helper"
	"getir-challenge/api/types"
)

func TestFailedResponseHandler(t *testing.T) {

	errMsg := "api error"
	w := httptest.NewRecorder()
	helper.FailedResponseHandler(
		w,
		http.StatusNotFound,
		helper.ResponseCodeErrorInMemoryDbKeyNotFound,
		errMsg)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusNotFound {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusNotFound)
	}

	var response types.GeneralResponseType
	err := json.Unmarshal(body, &response)
	if err != nil {
		t.Error("Could not unmarshall", err)
	}

	if response.Code != helper.ResponseCodeErrorInMemoryDbKeyNotFound {
		t.Errorf("Response code got %d, want %d", response.Code, helper.ResponseCodeErrorInMemoryDbKeyNotFound)
	}

	if response.Msg != errMsg {
		t.Errorf("Response msg got %s, want %s", response.Msg, errMsg)
	}
}

func TestSuccessResponseHandler(t *testing.T) {

	payload := types.InMemoryResponseType{
		Code: helper.ResponseCodeSuccess,
		Msg:  "Success",
	}
	w := httptest.NewRecorder()
	helper.SuccessResponseHandler(w, payload)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusOK)
	}

	var response types.GeneralResponseType
	err := json.Unmarshal(body, &response)
	if err != nil {
		t.Error("Could not unmarshall", err)
	}

	if response.Code != helper.ResponseCodeSuccess {
		t.Errorf("Response code got %d, want %d", response.Code, helper.ResponseCodeSuccess)
	}

	if response.Msg != payload.Msg {
		t.Errorf("Response msg got %s, want %s", response.Msg, payload.Msg)
	}
}
