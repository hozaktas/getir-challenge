package api_test

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"getir-challenge/api"
	"getir-challenge/api/helper"
	"getir-challenge/api/types"
	"getir-challenge/storage"
)

const TestMongodbURI = "mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true"
const TestMongodbTimeout = time.Second
const TestMongodbName = "getir-case-study"

func TestGetRecordsHandler(t *testing.T) {

	request := types.RecordsGetRequestType{
		StartDate: "2016-01-01",
		EndDate:   "2016-05-01",
		MinCount:  2000,
		MaxCount:  3000,
	}
	requestPayload, _ := json.Marshal(request)

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/mongo/records", bytes.NewReader(requestPayload))
	w := httptest.NewRecorder()

	mongoDbClient, err := storage.NewMongoDbStorage(TestMongodbURI, TestMongodbTimeout)
	if err != nil {
		t.Fatal("Could not mongoDb storage", err)
	}
	defer mongoDbClient.Disconnect(context.Background())

	handler := api.GetRecordsHandler(mongoDbClient, TestMongodbName)

	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusOK)
	}

	var recordsResponse types.RecordsResponseType
	err = json.Unmarshal(body, &recordsResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if recordsResponse.Code != helper.ResponseCodeSuccess {
		t.Errorf("Fail response code")
	}

	if recordsResponse.Msg != "Success" {
		t.Errorf("Response not success")
	}
}

func TestGetRecordsHandlerFailParams(t *testing.T) {

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/mongo/records", nil)
	w := httptest.NewRecorder()

	mongoDbClient, err := storage.NewMongoDbStorage(TestMongodbURI, TestMongodbTimeout)
	if err != nil {
		t.Fatal("Could not mongoDb storage", err)
	}
	defer mongoDbClient.Disconnect(context.Background())

	handler := api.GetRecordsHandler(mongoDbClient, TestMongodbName)

	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode == http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", http.StatusOK, resp.StatusCode)
	}

	var recordsResponse types.RecordsResponseType
	err = json.Unmarshal(body, &recordsResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if recordsResponse.Code == helper.ResponseCodeSuccess {
		t.Errorf("Fail response code")
	}

	if recordsResponse.Msg == "Success" {
		t.Errorf("Fail response success")
	}
}

func TestGetRecordsHandlerStartDateFail(t *testing.T) {

	request := types.RecordsGetRequestType{
		StartDate: "",
		EndDate:   "2016-05-01",
		MinCount:  2000,
		MaxCount:  3000,
	}
	requestPayload, _ := json.Marshal(request)

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/mongo/records", bytes.NewReader(requestPayload))
	w := httptest.NewRecorder()

	mongoDbClient, err := storage.NewMongoDbStorage(TestMongodbURI, TestMongodbTimeout)
	if err != nil {
		t.Fatal("Could not mongoDb storage", err)
	}
	defer mongoDbClient.Disconnect(context.Background())

	handler := api.GetRecordsHandler(mongoDbClient, TestMongodbName)

	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusBadRequest {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusBadRequest)
	}

	var recordsResponse types.RecordsResponseType
	err = json.Unmarshal(body, &recordsResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if recordsResponse.Code == helper.ResponseCodeSuccess {
		t.Errorf("Fail response code")
	}

	if recordsResponse.Msg == "Success" {
		t.Errorf("Fail response success")
	}
}

func TestGetRecordsHandlerEndDateFail(t *testing.T) {

	request := types.RecordsGetRequestType{
		StartDate: "2016-01-01",
		EndDate:   "",
		MinCount:  2000,
		MaxCount:  3000,
	}
	requestPayload, _ := json.Marshal(request)

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/mongo/records", bytes.NewReader(requestPayload))
	w := httptest.NewRecorder()

	mongoDbClient, err := storage.NewMongoDbStorage(TestMongodbURI, TestMongodbTimeout)
	if err != nil {
		t.Fatal("Could not mongoDb storage", err)
	}
	defer mongoDbClient.Disconnect(context.Background())

	handler := api.GetRecordsHandler(mongoDbClient, TestMongodbName)

	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusBadRequest {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusBadRequest)
	}

	var recordsResponse types.RecordsResponseType
	err = json.Unmarshal(body, &recordsResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if recordsResponse.Code == helper.ResponseCodeSuccess {
		t.Errorf("Fail response code")
	}

	if recordsResponse.Msg == "Success" {
		t.Errorf("Fail response success")
	}
}

func TestGetRecordsHandlerStartEndDateDiffFail(t *testing.T) {

	request := types.RecordsGetRequestType{
		StartDate: "2016-01-01",
		EndDate:   "2015-01-01",
		MinCount:  2000,
		MaxCount:  3000,
	}
	requestPayload, _ := json.Marshal(request)

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/mongo/records", bytes.NewReader(requestPayload))
	w := httptest.NewRecorder()

	mongoDbClient, err := storage.NewMongoDbStorage(TestMongodbURI, TestMongodbTimeout)
	if err != nil {
		t.Fatal("Could not mongoDb storage", err)
	}
	defer mongoDbClient.Disconnect(context.Background())

	handler := api.GetRecordsHandler(mongoDbClient, TestMongodbName)

	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusBadRequest {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusBadRequest)
	}

	var recordsResponse types.RecordsResponseType
	err = json.Unmarshal(body, &recordsResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if recordsResponse.Code == helper.ResponseCodeSuccess {
		t.Errorf("Fail response code")
	}

	if recordsResponse.Msg == "Success" {
		t.Errorf("Fail response success")
	}
}

func TestGetRecordsHandlerMinMaxCountDiffFail(t *testing.T) {

	request := types.RecordsGetRequestType{
		StartDate: "2016-01-01",
		EndDate:   "2016-01-02",
		MinCount:  6000,
		MaxCount:  3000,
	}
	requestPayload, _ := json.Marshal(request)

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/mongo/records", bytes.NewReader(requestPayload))
	w := httptest.NewRecorder()

	mongoDbClient, err := storage.NewMongoDbStorage(TestMongodbURI, TestMongodbTimeout)
	if err != nil {
		t.Fatal("Could not mongoDb storage", err)
	}
	defer mongoDbClient.Disconnect(context.Background())

	handler := api.GetRecordsHandler(mongoDbClient, TestMongodbName)

	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusBadRequest {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusBadRequest)
	}

	var recordsResponse types.RecordsResponseType
	err = json.Unmarshal(body, &recordsResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if recordsResponse.Code == helper.ResponseCodeSuccess {
		t.Errorf("Fail response code")
	}

	if recordsResponse.Msg == "Success" {
		t.Errorf("Fail response success")
	}
}

func TestInMemoryDbHandler(t *testing.T) {

	inMemoryDb := storage.NewInMemoryStorage()
	request := types.InMemorySetRequestType{
		Key:   "foo",
		Value: "bar",
	}
	requestPayload, _ := json.Marshal(request)

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/memory/create", bytes.NewReader(requestPayload))
	w := httptest.NewRecorder()

	handler := api.SetInMemoryDbHandler(inMemoryDb)
	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusOK)
	}

	var inMemoryResponse types.InMemoryResponseType
	err := json.Unmarshal(body, &inMemoryResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if inMemoryResponse.Msg != "Success" {
		t.Errorf("Response not success")
	}

	req = httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/memory/fetchdata?key=foo", nil)
	w = httptest.NewRecorder()

	handler = api.GetInMemoryDbHandler(inMemoryDb)
	handler.ServeHTTP(w, req)

	resp = w.Result()
	body, _ = ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusOK)
	}

	err = json.Unmarshal(body, &inMemoryResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if inMemoryResponse.Msg != "Success" {
		t.Errorf("Response not success")
	}

	if inMemoryResponse.Value != "bar" {
		t.Errorf("Value got %s, want %s", inMemoryResponse.Value, "bar")
	}
}

func TestInMemoryDbHandlerFail(t *testing.T) {
	inMemoryDb := storage.NewInMemoryStorage()
	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/memory/fetchdata?key=fail", nil)
	w := httptest.NewRecorder()

	handler := api.GetInMemoryDbHandler(inMemoryDb)
	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusOK)
	}

	var inMemoryResponse types.InMemoryResponseType
	err := json.Unmarshal(body, &inMemoryResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if inMemoryResponse.Msg == "Success" {
		t.Errorf("Response not success")
	}
}

func TestInMemoryDbHandlerEmptyFail(t *testing.T) {
	inMemoryDb := storage.NewInMemoryStorage()
	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/memory/fetchdata?", nil)
	w := httptest.NewRecorder()

	handler := api.GetInMemoryDbHandler(inMemoryDb)
	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusBadRequest {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusBadRequest)
	}

	var inMemoryResponse types.InMemoryResponseType
	err := json.Unmarshal(body, &inMemoryResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if inMemoryResponse.Msg == "Success" {
		t.Errorf("Response not success")
	}
}

func TestSetInMemoryDbHandlerFailParam(t *testing.T) {

	inMemoryDb := storage.NewInMemoryStorage()

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/memory/create", nil)
	w := httptest.NewRecorder()

	handler := api.SetInMemoryDbHandler(inMemoryDb)
	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode == http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", http.StatusOK, resp.StatusCode)
	}

	var inMemoryResponse types.InMemoryResponseType
	err := json.Unmarshal(body, &inMemoryResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if inMemoryResponse.Msg == "Success" {
		t.Errorf("Response not success")
	}
}

func TestSetInMemoryDbHandlerFailEmptyParam(t *testing.T) {

	inMemoryDb := storage.NewInMemoryStorage()
	request := types.InMemorySetRequestType{
		Key:   "",
		Value: "bar",
	}
	requestPayload, _ := json.Marshal(request)

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/memory/create", bytes.NewReader(requestPayload))
	w := httptest.NewRecorder()

	handler := api.SetInMemoryDbHandler(inMemoryDb)
	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode == http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", http.StatusOK, resp.StatusCode)
	}

	var inMemoryResponse types.InMemoryResponseType
	err := json.Unmarshal(body, &inMemoryResponse)
	if err != nil {
		t.Error("Not unmarshall response", err)
	}

	if inMemoryResponse.Msg == "Success" {
		t.Errorf("Response not success")
	}
}
