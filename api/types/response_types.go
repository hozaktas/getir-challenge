package types

import "time"

// RecordItemType used in requests api responses for "Mongo Db"
type RecordItemType struct {
	CreatedAt time.Time `json:"createdAt" bson:"createdAt"`
	Key       string    `json:"key" bson:"_id"`
	Count     int       `json:"totalCount" bson:"totalCount"`
}

// RecordsResponseType used in requests api responses for "Mongo Db"
type RecordsResponseType struct {
	Code    int              `json:"code"`
	Msg     string           `json:"msg"`
	Records []RecordItemType `json:"records"`
}

// InMemoryResponseType used in requests api responses for "In Memory Db"
type InMemoryResponseType struct {
	Code  int    `json:"code"`
	Msg   string `json:"msg"`
	Key   string `json:"key"`
	Value string `json:"value"`
}
