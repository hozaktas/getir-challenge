package types

// GeneralResponseType only used in api ends that return error code and message
type GeneralResponseType struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}
