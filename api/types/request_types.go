package types

// RecordsGetRequestType requests for "records" data via mongodb
type RecordsGetRequestType struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
	MinCount  int    `json:"minCount"`
	MaxCount  int    `json:"maxCount"`
}

// InMemorySetRequestType incoming set requests struct for "In Memory Db"
type InMemorySetRequestType struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// InMemoryGetRequestType incoming fetch requests struct for "In Memory Db"
type InMemoryGetRequestType struct {
	Key string `json:"key"`
}
