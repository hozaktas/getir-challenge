package api

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"getir-challenge/api/helper"
	"getir-challenge/api/types"
	"getir-challenge/api/validator"
	"getir-challenge/storage"
)

// GetRecordsHandler only POST method, returns records from mongodb according to specified filters
func GetRecordsHandler(mongoDbClient *mongo.Client, dbName string) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {

		var recordsGetRequest types.RecordsGetRequestType
		err := json.NewDecoder(req.Body).Decode(&recordsGetRequest)
		if err != nil {
			helper.FailedResponseHandler(
				res,
				http.StatusBadRequest,
				helper.ResponseCodeErrorInvalidRequest,
				"Invalid request param(s).")

			// Incoming bad requests are logged
			log.Println("GetRecords: Invalid request param(s).", recordsGetRequest)
			return
		}

		startDate, err := validator.ValidateDateString(recordsGetRequest.StartDate)
		if err != nil {
			helper.FailedResponseHandler(
				res,
				http.StatusBadRequest,
				helper.ResponseCodeErrorRecordsStartdateFormat,
				"Invalid startDate.")

			// Incoming bad requests are logged
			log.Println("GetRecords: Invalid startDate.", startDate, recordsGetRequest)
			return
		}

		endDate, err := validator.ValidateDateString(recordsGetRequest.EndDate)
		if err != nil {
			helper.FailedResponseHandler(
				res,
				http.StatusBadRequest,
				helper.ResponseCodeErrorRecordsEnddateFormat,
				"Invalid endDate.")

			// Incoming bad requests are logged
			log.Println("GetRecords: Invalid endDate.", endDate, recordsGetRequest)
			return
		}

		// date diff err
		if startDate.Unix() > endDate.Unix() {
			helper.FailedResponseHandler(
				res,
				http.StatusBadRequest,
				helper.ResponseCodeErrorRecordsStartEndDiff,
				"Invalid Between StartDate <> EndDate.")

			// Incoming bad requests are logged
			log.Println("GetRecords: Invalid Between StartDate <> EndDate.", endDate, recordsGetRequest)
			return
		}

		// minCount <> maxCount diff err
		if recordsGetRequest.MinCount > 0 && recordsGetRequest.MinCount > recordsGetRequest.MaxCount {
			helper.FailedResponseHandler(
				res,
				http.StatusBadRequest,
				helper.ResponseCodeErrorRecordsMinMaxDiff,
				"Invalid Between MinCount <> MaxCount.")

			// Incoming bad requests are logged
			log.Println("GetRecords: Invalid Between MinCount <> MaxCount", endDate, recordsGetRequest)
			return
		}

		pipeline := make([]bson.D, 0, 4)
		pipelineProject := bson.D{
			primitive.E{
				Key: "$project", Value: bson.D{
					primitive.E{Key: "_id", Value: 0},
					primitive.E{Key: "key", Value: 1},
					primitive.E{Key: "createdAt", Value: 1},
					primitive.E{Key: "totalCount", Value: bson.D{
						primitive.E{Key: "$sum", Value: "$counts"}}},
				}},
		}
		pipelineMatch := bson.D{
			primitive.E{
				Key: "$match", Value: bson.D{
					bson.E{Key: "createdAt", Value: bson.D{
						primitive.E{Key: "$gte", Value: startDate},
						primitive.E{Key: "$lte", Value: endDate},
					}},
					bson.E{Key: "totalCount", Value: bson.D{
						primitive.E{Key: "$gte", Value: recordsGetRequest.MinCount},
						primitive.E{Key: "$lte", Value: recordsGetRequest.MaxCount},
					}},
				}},
		}
		pipelineGroup := bson.D{
			primitive.E{
				Key: "$group", Value: bson.D{
					primitive.E{Key: "_id", Value: "$key"},
					primitive.E{Key: "createdAt", Value: bson.D{primitive.E{Key: "$first", Value: "$createdAt"}}},
					primitive.E{Key: "totalCount", Value: bson.D{primitive.E{
						Key: "$sum", Value: "$totalCount",
					}}},
				}},
		}
		pipelineSort := bson.D{
			primitive.E{Key: "$sort", Value: bson.D{
				primitive.E{Key: "createdAt", Value: 1}},
			},
		}

		pipeline = append(pipeline, pipelineProject, pipelineMatch, pipelineGroup, pipelineSort)
		recordCollection := mongoDbClient.Database(dbName).Collection(storage.COLLECTION_NAME)
		recordsCursor, _ := recordCollection.Aggregate(context.Background(), pipeline)
		var records []types.RecordItemType
		if err := recordsCursor.All(context.Background(), &records); err != nil {
			helper.FailedResponseHandler(
				res,
				http.StatusInternalServerError,
				helper.ResponseCodeErrorRecordsDbErr,
				"Pipeline err")

			// Incoming bad requests are logged
			log.Println("GetRecords: Pipeline err", recordsGetRequest)
			return
		}

		helper.SuccessResponseHandler(res, types.RecordsResponseType{
			Code:    0,
			Msg:     "Success",
			Records: records,
		})
	}
}

// SetInMemoryDbHandler only POST method, incoming set requests for "In Memory Db"
func SetInMemoryDbHandler(inMemoryDb *storage.InMemoryDb) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		var inMemorySetRequest types.InMemorySetRequestType
		err := json.NewDecoder(req.Body).Decode(&inMemorySetRequest)
		if err != nil {
			helper.FailedResponseHandler(
				res,
				http.StatusBadRequest,
				helper.ResponseCodeErrorInvalidRequest,
				"Invalid request param(s).")

			// Incoming bad requests are logged
			log.Println("SetInMemoryDb: Invalid request param(s).", inMemorySetRequest)
			return
		}

		if len(inMemorySetRequest.Key) == 0 || len(inMemorySetRequest.Value) == 0 {
			helper.FailedResponseHandler(
				res,
				http.StatusBadRequest,
				helper.ResponseCodeErrorInvalidRequest,
				"Invalid request param(s). Key or Value length zero.")

			// Incoming bad requests are logged
			log.Println("SetInMemoryDb: Invalid request param(s). Key or Value length zero.", inMemorySetRequest)
			return
		}

		inMemoryDb.Set(inMemorySetRequest.Key, inMemorySetRequest.Value)

		helper.SuccessResponseHandler(res, types.InMemoryResponseType{
			Code:  0,
			Msg:   "Success",
			Key:   inMemorySetRequest.Key,
			Value: inMemorySetRequest.Value,
		})
	}
}

// GetInMemoryDbHandler only GET method, incoming fetch requests for "In Memory Db"
func GetInMemoryDbHandler(inMemoryDb *storage.InMemoryDb) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {

		key := req.URL.Query().Get("key")
		if key == "" {
			helper.FailedResponseHandler(
				res,
				http.StatusBadRequest,
				helper.ResponseCodeErrorInvalidRequest,
				"Invalid request param(s). Key param not found.")

			// Incoming bad requests are logged
			log.Println("GetInMemoryDb: Invalid request param(s). Key param not found.")
			return
		}

		value, err := inMemoryDb.Get(key)
		if err != nil {
			helper.FailedResponseHandler(
				res,
				http.StatusOK,
				helper.ResponseCodeErrorInMemoryDbKeyNotFound,
				"Data not found.")

			// requests of non-existent keys are logged
			log.Println("GetInMemoryDb: Data not found.", key)
			return
		}

		helper.SuccessResponseHandler(res, types.InMemoryResponseType{
			Code:  0,
			Msg:   "Success",
			Key:   key,
			Value: value,
		})
	}
}
