package validator

import (
	"errors"
	"time"
)

// ValidateDateString checks date data in string format
func ValidateDateString(dateString string) (time.Time, error) {
	var date time.Time
	date, err := time.Parse("2006-01-02", dateString)
	if err != nil {
		return date, errors.New("Invalid date string, Format: YYYY-mm-dd")
	}
	return date, nil
}
