package validator_test

import (
	"testing"
	"time"

	"getir-challenge/api/validator"
)

func TestValidateDateStringValid(t *testing.T) {
	date, _ := validator.ValidateDateString("2021-06-06")
	if date.Year() != 2021 {
		t.Errorf("Year got %d, want %d", date.Year(), 2021)
	}

	if date.Month() != time.June {
		t.Errorf("Month got %d, want %d", date.Month(), time.June)
	}

	if date.Day() != 6 {
		t.Errorf("Day got %d, want %d", date.Day(), 6)
	}
}

func TestValidateDateStringFail(t *testing.T) {
	_, err := validator.ValidateDateString("2021-30-06")
	if err == nil {
		t.Error("2021-30-06 is not time.Time")
	}

	_, err = validator.ValidateDateString("")
	if err == nil {
		t.Error("empty time.Time")
	}
}
