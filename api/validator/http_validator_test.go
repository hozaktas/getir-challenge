package validator_test

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"getir-challenge/api/validator"
)

func TestValidateHTTPMethod(t *testing.T) {

	req := httptest.NewRequest(http.MethodGet, "http://127.0.0.1:8080/memory/fetchdata?key=active-tabs", nil)
	w := httptest.NewRecorder()

	payload := `{"key": "value"}`

	handler := validator.ValidateHTTPMethod(http.MethodGet, func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, payload)
	})

	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusOK)
	}

	if string(body) != payload {
		t.Errorf("Body got %s, want %s", string(body), payload)
	}
}

func TestValidateHTTPMethodFail(t *testing.T) {

	req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1:8080/memory/fetchdata?key=active-tabs", nil)
	w := httptest.NewRecorder()

	payload := `{"key": "value"}`

	handler := validator.ValidateHTTPMethod(http.MethodGet, func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, payload)
	})

	handler.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusMethodNotAllowed {
		t.Errorf("StatusCode got %d, want %d", resp.StatusCode, http.StatusMethodNotAllowed)
	}

	if string(body) == payload {
		t.Errorf("Body got %s, want not %s", string(body), payload)
	}
}
