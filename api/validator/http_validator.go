package validator

import (
	"log"
	"net/http"

	"getir-challenge/api/helper"
)

// ValidateHTTPMethod it is the place where the request methods are checked before the incoming requests are processed in the API.
func ValidateHTTPMethod(method string, next http.HandlerFunc) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		if req.Method != method {
			helper.FailedResponseHandler(
				res,
				http.StatusMethodNotAllowed,
				helper.ResponseCodeErrorInvalidMethod,
				"Invalid request method.")

			// Incoming bad requests are logged
			log.Println("ValidateHttpMethod: Invalid request method.")
			return
		}
		next(res, req)
	}
}
