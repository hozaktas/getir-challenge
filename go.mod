module getir-challenge

go 1.15

require (
	github.com/joho/godotenv v1.3.0
	go.mongodb.org/mongo-driver v1.5.3
)
