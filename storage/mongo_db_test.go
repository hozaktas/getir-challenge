package storage_test

import (
	"context"
	"testing"
	"time"

	"getir-challenge/storage"
)

const TEST_MONGODB_URI = "mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true"
const TEST_MONGODB_FAIL_URI = "mongodb+srv://127.0.01"
const TEST_MONGODB_TIMEOUT = time.Second

func TestNewMongoDbStorage(t *testing.T) {
	mongoDbClient, err := storage.NewMongoDbStorage(TEST_MONGODB_URI, TEST_MONGODB_TIMEOUT)
	if err != nil {
		t.Fatal("Could not mongoDb storage", err)
	}
	mongoDbClient.Disconnect(context.Background())
}

func TestNewMongoDbStorageFail(t *testing.T) {
	_, err := storage.NewMongoDbStorage(TEST_MONGODB_FAIL_URI, TEST_MONGODB_TIMEOUT)
	if err == nil {
		t.Fatal("Could not mongoDb storage", err)
	}
}
