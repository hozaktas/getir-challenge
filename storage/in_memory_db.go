package storage

import (
	"errors"
	"sync"
)

// ErrKeyNotFound is returned when requested key not found
var ErrKeyNotFound = errors.New("inMemoryDb: requested key not found")

// InMemoryDb struct for keys stored in memory
type InMemoryDb struct {
	valuesMutex sync.Mutex // protects following
	values      map[string]string
}

// NewInMemoryStorage creates memory for "inMemoryDb"
func NewInMemoryStorage() *InMemoryDb {
	values := make(map[string]string)
	return &InMemoryDb{
		values: values,
	}
}

// Get returns the value of the given key
func (inMemoryDb *InMemoryDb) Get(key string) (string, error) {
	inMemoryDb.valuesMutex.Lock()
	defer inMemoryDb.valuesMutex.Unlock()

	val, ok := inMemoryDb.values[key]
	if !ok {
		return "", ErrKeyNotFound
	}
	return val, nil
}

// Set saves data in key value format
func (inMemoryDb *InMemoryDb) Set(key string, value string) {
	inMemoryDb.valuesMutex.Lock()
	defer inMemoryDb.valuesMutex.Unlock()
	inMemoryDb.values[key] = value
}
