package storage

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const COLLECTION_NAME = "records"

// NewMongoDbStorage opens new mongodb connection
func NewMongoDbStorage(mongoDbURI string, mongoDbTimeOut time.Duration) (*mongo.Client, error) {
	mongoDbClient, err := mongo.NewClient(options.Client().ApplyURI(mongoDbURI))
	if err != nil {
		return nil, fmt.Errorf("Could not create mongoDbClient: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), mongoDbTimeOut)
	defer cancel()

	err = mongoDbClient.Connect(ctx)
	if err != nil {
		return nil, fmt.Errorf("Could not connect mongoDbClient: %w", err)
	}

	return mongoDbClient, nil
}
