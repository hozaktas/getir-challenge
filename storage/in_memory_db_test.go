package storage_test

import (
	"testing"

	"getir-challenge/storage"
)

func TestInMemoryStorage(t *testing.T) {
	inMemoryDb := storage.NewInMemoryStorage()
	_, err := inMemoryDb.Get("foo")
	if err != storage.ErrKeyNotFound {
		t.Errorf("Error got %s, want %s", err, storage.ErrKeyNotFound)
	}

	inMemoryDb.Set("foo", "bar")
	value, err := inMemoryDb.Get("foo")
	if value != "bar" {
		t.Errorf("Value got %s, want %s", value, "bar")
	}

	if err != nil {
		t.Errorf("Error got %s, want nil", err)
	}
}

func BenchmarkInMemoryDbGet(b *testing.B) {
	inMemoryDb := storage.NewInMemoryStorage()
	inMemoryDb.Set("foo", "bar")
	for n := 0; n < b.N; n++ {
		inMemoryDb.Get("foo")
	}
}

func BenchmarkInMemoryDbSet(b *testing.B) {
	inMemoryDb := storage.NewInMemoryStorage()
	for n := 0; n < b.N; n++ {
		inMemoryDb.Set("foo", "bar")
	}
}
